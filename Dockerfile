FROM node:13-alpine AS builder

COPY . /app
WORKDIR /app

RUN npm install
RUN npm run build


FROM nginx
MAINTAINER celestian "petr.celestian@gmail.com"

COPY --from=builder /app/build /usr/share/nginx/html
